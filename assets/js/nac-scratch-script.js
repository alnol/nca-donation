createElement();

function createElement(){
  var el = document.getElementsByClassName("container-nac-scratch")[0];
  if(el)
    el.innerHTML += generateElement();
}

var container = document.getElementById("nac-scratch-container"),
bridge = document.getElementById("nac-scratch-frame"),
bridgeCanvas = bridge.getContext('2d'),
brushRadius = (bridge.width / 100) * 5,
img = new Image(),
dataURL = bridge.toDataURL(),
number = getNumber(),
frame = document.getElementById('nac-scratch-frame');

if (brushRadius < 50) { brushRadius = 50 }

// SET INITIAL VALUE
setFrameImage(number);
setResponsiveFrame(number);
setCopyright(number);


img.onload = function(){  
  bridgeCanvas.drawImage(img, 0, 0, bridge.width, bridge.height);
}
img.loc = 'assets/img/';
img.filename = 'frame-'+ number +'-grey.png';
img.src = img.loc + img.filename;

function detectLeftButton(event) {
  if ('buttons' in event) {
    return event.buttons === 1;
  } else if ('which' in event) {
    return event.which === 1;
  } else {
    return event.button === 1;
  }
}

function getBrushPos(xRef, yRef) {
  var bridgeRect = bridge.getBoundingClientRect();
  var axisY = 0, axisX = 0;

  var win1024 = window.matchMedia( "(max-width: 1024px)" );
  if(win1024.matches){
    axisY = Math.floor(((yRef-bridgeRect.top - (window.pageYOffset))/(bridgeRect.bottom-bridgeRect.top)*bridge.height));
  }else{
    axisY = Math.floor((yRef-bridgeRect.top)/(bridgeRect.bottom-bridgeRect.top)*bridge.height);
  }

  axisX = Math.floor((xRef-bridgeRect.left)/(bridgeRect.right-bridgeRect.left)*bridge.width);

  return {
    x: axisX,
    y: axisY
  };
}

function drawDot(mouseX,mouseY){
  bridgeCanvas.beginPath();
  bridgeCanvas.arc(mouseX, mouseY, brushRadius, 0, 2*Math.PI, true);
  bridgeCanvas.fillStyle = '#000';
  bridgeCanvas.globalCompositeOperation = "destination-out";
  bridgeCanvas.fill();

  var imageData = bridgeCanvas.getImageData(0, 0, bridgeCanvas.canvas.width, bridgeCanvas.canvas.height);
  var trans = 0;
  for (var i = 0; i < imageData.data.length; i += 4) {
    if (imageData.data[3 + i] === 0) {
      trans++
    }
  }
  var percent = trans / (imageData.data.length / 4) * 100;

  // 90% Scratched Conditions
  if(percent >= '90'){
    var myClasses = document.querySelectorAll('.nac-scratch-finish'),
    i = 0,
    l = myClasses.length;

    for (i; i < l; i++) {
      myClasses[i].style.display = 'block';
    }
  }
}

container.addEventListener("click", function(e) {
  var myClasses = container.querySelectorAll('.nac-scratch-start'),
  i = 0,
  l = myClasses.length;

  for (i; i < l; i++) {
    myClasses[i].style.display = 'none';
  }
});

bridge.addEventListener("mousemove", function(e) {
  var brushPos = getBrushPos(e.clientX, e.clientY);
  var leftBut = detectLeftButton(e);
  if (leftBut == 1) {
    drawDot(brushPos.x, brushPos.y);
  }
}, false);

bridge.addEventListener("touchmove", function(e) {
  e.preventDefault();
  e.stopPropagation();
  var touch = e.targetTouches[0];
  if (touch) {
    var brushPos = getBrushPos(touch.pageX, touch.pageY);
    drawDot(brushPos.x, brushPos.y);
  }
}, false);

function getNumber(){
  return Math.floor(Math.random() * 3);
}

function setFrameImage(number){
  frame.style.backgroundImage = "url('assets/img/frame-"+ number +".png')";

  if(number == "0"){
    frame.style.height = "234px";
    frame.style.top = "120px";
  }else{
    frame.style.height = "auto";
    frame.style.bottom = "22px";
  }
}

function setCopyright(number){
  let copytext = '';

  switch(number){
    case 1:
      copytext = "Mojoko, Escape to Jurong";
      break;
    case 2:
      copytext = "Muneera Malek, Zouk at Jiak Kim Street";
      break;
    default:
      copytext = "Kamal Dollah, Blu Jaz Cafe, Singapore";
  }

  const copyEl = document.getElementsByClassName('image-copyright')[0];
  copyEl.innerHTML = copytext;
}

function setResponsiveFrame(number){
  var win1024 = window.matchMedia( "(max-width: 1024px)" );
  if(win1024.matches){
    if(number == "0"){
      frame.style.bottom = "110px";
      frame.style.top = "unset";
    }
  }

  var win375 = window.matchMedia( "(max-width: 375px)" );
  if(win375.matches){
    if(number == "0"){
      frame.style.bottom = "125px";
      frame.style.top = "unset";
    }
  }
}

function generateElement(){
  return "<div id=\"nac-scratch-container\">\
  <div id=\"nac-scratch-logo\">\
    <img src=\"assets/img/logo.png\" />\
  </div>\
  <div id=\"nac-scratch-intro\" class=\"nac-scratch-start\">\
    <div class=\"nac-scratch-intro-content\">\
      <h1>SCRATCH</h1>\
      <h3>to bring colour to the arts</h3>\
      <img class=\"nac-scratch-intro-mouse\" src=\"assets/img/mouse.gif\" />\
    </div>\
  </div>\
  <div id=\"nac-scratch-outro\" class=\"nac-scratch-finish\"></div>\
  <div class=\"nac-scratch-finish-text-left nac-scratch-finish\">\
    <h3>OUR LOCAL ARTS SCENE</h3>\
    <h5>NEEDS YOUR HELP.</h5>\
  </div>\
  <canvas id=\"nac-scratch-frame\" width=\"312\" height=\"436\"></canvas>\
  <div class=\"image-copyright\"></div>\
  <div class=\"nac-scratch-finish\">\
    <div class=\"nac-scratch-finish-text-right\">\
      <img src=\"assets/img/qr-donation.png\" />\
      <h5>Scan the QR Code or click <a href=\"http://giving.sg/startfund\" class=\"link-donation\" target=\"_blank\">here</a> to donate now.</h5>\
    </div>\
  </div>\
  <div id=\"nac-scratch-canvas-stand\"></div>\
</div>";
}